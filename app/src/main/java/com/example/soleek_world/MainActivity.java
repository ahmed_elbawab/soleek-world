package com.example.soleek_world;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;

    private Button loginButton;
    private Button registerButton;
    private Button firebaseUiButton;

    private int RC_SIGN_IN = 505;

    // TODO : Fix google sign in
    // TODO : Fix facebook sign in
    // TODO : add manual log in
    // TODO : verify emails

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        // Check if the user is already loged
        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(MainActivity.this, CountriesActivity.class));
            finish();
        }

        loginButton = findViewById(R.id.mainActivitySigninButton);
        registerButton = findViewById(R.id.mainActivityRegisterButton);
        firebaseUiButton = findViewById(R.id.mainActivityFirebaseUiButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, RegisterActivity.class));
            }
        });

        firebaseUiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Call Firebase UI Auth to start sign-in process
                signinUser();
            }
        });
    }

    private void signinUser(){
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.PhoneBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build(),
                new AuthUI.IdpConfig.FacebookBuilder().build());


        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)

                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                // Successfully signed in
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                // ...
                Toast.makeText(getApplicationContext(),"signed in successfully",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, CountriesActivity.class));
                finish();
            } else {
                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
                Toast.makeText(getApplicationContext(),"Sign in failed!",Toast.LENGTH_SHORT).show();
            }
        }
    }
}

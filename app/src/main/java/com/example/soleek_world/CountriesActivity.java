package com.example.soleek_world;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class CountriesActivity extends AppCompatActivity {

    private ArrayList<String> countries = new ArrayList<>();

    private ListView listView;

    private String url = "https://restcountries.eu/rest/v2/all";

    private Button logout;

    // TODO : use loader to save data on rotate
    // TODO : use add option to use retrofit
    // TODO : add log uot button

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);

        listView = findViewById(R.id.listview);

        logout = findViewById(R.id.countriesActivityLogoutButton);

        CountriesLoader countriesLoader = new CountriesLoader();

        try {
            countries = countriesLoader.execute(url).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, countries);

        listView.setAdapter(adapter);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(CountriesActivity.this, MainActivity.class));
                finish();
            }
        });
    }
}
